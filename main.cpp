#include <wx/app.h>
#include <wx/frame.h>

class MyFrame: public wxFrame
{
public:
  MyFrame(): wxFrame(NULL, wxID_ANY, "balls")
  {
    
  }
};

class MyApp: public wxApp
{
public:
  bool OnInit()
  {
    MyFrame* frame = new MyFrame();
    frame->Show(true);
    return true;
  }
};

wxIMPLEMENT_APP(MyApp);
