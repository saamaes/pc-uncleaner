SHELL = /bin/sh

WXFLAGS := -I./include
WXLIBS := -L./lib/gcc1020_x64_dll/ -lwxbase31u -lwxmsw31u_core

CXX = x86_64-w64-mingw32-g++
CXXFLAGS = $(WXFLAGS) -Wpedantic -Wall -Wextra -g
LDFLAGS = $(WXLIBS)

PCuncleaner.exe : main.o
	$(CXX) -o $@ $^ $(LDFLAGS)

main.o : main.cpp
	$(CXX) $(CXXFLAGS) -c $^

clean :
	rm -fv main.o PCuncleaner.exe
